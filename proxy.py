#!/usr/bin/env python3

import asyncio
import dbus_next
from dbus_next.aio import MessageBus
import aiocoap

import logging

import coap_ble_common

async def interface(bus, bus_name, path, interface_name):
    introspection = await bus.introspect(bus_name, path)
    return bus.get_proxy_object(bus_name, path, introspection).get_interface(interface_name)

async def discover(log, bus):
    """Find a CoAP-over-Bluetooth capable device

    Right now this returns a single one; could later be extended to return more
    or support further filtering."""

    adapters = []
    found_coap = asyncio.Queue()

    root = await interface(bus, 'org.bluez', '/', 'org.freedesktop.DBus.ObjectManager')

    def check_object(path, interfaces):
        if 'org.bluez.Adapter1' in data:
            adapters.append(path)
            return

        if 'org.bluez.Device1' not in interfaces:
            return

        uuids = interfaces['org.bluez.Device1']['UUIDs'].value
        if coap_ble_common.coap_us not in uuids:
            return

        log.info("Found device %s which advertises CoAP", path)
        found_coap.put_nowait((path, interfaces))

    root.on_interfaces_added(check_object)

    objects = await root.call_get_managed_objects()

    for (path, data) in objects.items():
        check_object(path, data)

    if not found_coap.empty():
        return found_coap.get_nowait()

    log.info("Nothing found so far, turning on the probing")

    # not going the extra mile of trying to bring up adapters discovered later
    # -- instead cutting the line here to ensure late-plugged-in adapters don't
    # get a stop_discovery without a start_discovery. (not that it'd be overly
    # hard to shape things that way, but that'd lend itself more to a setup
    # where scanning is started right away, and i'dd prefer to keep it quiet
    # and process already known devices first).
    used_adapters = adapters[:]
    # assuming they all have the same -- should make sense given that the
    # general dbus-next assumption is that the application ships the XML.
    interface_introspection = await bus.introspect('org.bluez', used_adapters[0])
    used_adapters = [bus.get_proxy_object(
                'org.bluez',
                a,
                interface_introspection
            ).get_interface('org.bluez.Adapter1')
            for a in used_adapters]

    await asyncio.gather(*(a.call_set_discovery_filter({
        "UUIDs": dbus_next.Variant("as", [coap_ble_common.coap_us]),
        "Transport": dbus_next.Variant("s", "le"),
        }) for a in used_adapters))

    try:
        await asyncio.gather(*(a.call_start_discovery() for a in used_adapters))

        return await found_coap.get()
    finally:
        await asyncio.gather(*(a.call_stop_discovery() for a in used_adapters))

async def device_to_characteristics(log, bus, device):
    (path, interfaces) = device
    device_properties = interfaces['org.bluez.Device1']

    assert coap_ble_common.coap_us in device_properties['UUIDs'].value

    log.info("Working with %r at address %s", device_properties['Name'].value, device_properties['Address'].value)
    epname = device_properties['Address'].value.replace(':', '').lower()

    device = await interface(bus, 'org.bluez', path, 'org.bluez.Device1')

    connected = device_properties['Connected'].value

    if not connected:
        log.info("Not connected, connecting...")
        # not sure why this fails usually even though it effects a connection,
        # while a plain connect works
        #await device.call_connect_profile(coap_ble_common.coap_us)
        await device.call_connect()

    # now we should have descriptors available

    characteristics = []

    root = await interface(bus, 'org.bluez', '/', 'org.freedesktop.DBus.ObjectManager')
    for charpath, charinterfaces in (await root.call_get_managed_objects()).items():
        if 'org.bluez.GattCharacteristic1' not in charinterfaces:
            continue

        if charinterfaces['org.bluez.GattCharacteristic1']['UUID'].value != coap_ble_common.coap_uc:
            continue

        char = await interface(bus, 'org.bluez', charpath, 'org.bluez.GattCharacteristic1')
        characteristics.append(char)

    return characteristics, epname

async def play_with(log, bus, characteristics):
    try:
        char = characteristics[0]
    except IndexError:
        raise RuntimeError("Device is bad for advertising the profile but no single characteristic")

    async def request(msg):
        req = coap_ble_common.encode(msg)
        await char.call_write_value(req, {})
        res = bytes(await char.call_read_value({}))
        return coap_ble_common.decode(res)

    res = await request(aiocoap.Message(code=aiocoap.GET, uri_path=[".well-known", "core"]))
    log.info(f"Read response: {res} payload {res.payload}")

    res = await request(aiocoap.Message(code=aiocoap.GET, uri_path=["board"]))
    log.info(f"Read response: {res} payload {res.payload}")

    res = await request(aiocoap.Message(code=aiocoap.GET, uri_path=["led"]))
    log.info(f"Read response: {res} payload {res.payload}")

async def run_rdproxying(log, epname, characteristics, rd_uri):
    """Run a reverse proxy for the given CoAP-over-BLE characteristics, and do
    simple registration with a proxy request against the RD URI.

    The reverse proxying is made easy by running in client mode, effectively
    placing the server on a random port. The RD in proxy mode smooths over
    that anyway."""

    class BLEProxy:
        def __init__(self, characteristics):
            self.characteristics = characteristics
            # this is a bit restrictive, and doesn't use all the
            # characteristics, but hey it works
            self.lock = asyncio.Lock()

        async def needs_blockwise_assembly(self, request):
            # FIXME: actually we *do* need this in the end, we'll just need to
            # blockwise even further
            return False

        async def render(self, request):
            lock = await asyncio.wait_for(self.lock.acquire(), timeout=5)
            char = self.characteristics[0]
            try:
                # FIXME any options that may need processing?
                req = coap_ble_common.encode(request)
                await char.call_write_value(req, {})
                res = bytes(await char.call_read_value({}))
                return coap_ble_common.decode(res)
            finally:
                self.lock.release()

    ctx = await aiocoap.Context.create_client_context()
    ctx.serversite = BLEProxy(characteristics)

    # towards the RD
    timeout = 300

    while True:
        await ctx.request(aiocoap.Message(
            code=aiocoap.POST,
            uri=rd_uri,
            uri_query=["lt=%s" % timeout, 'proxy=on', 'ep='+ epname]
            )).response_raising
        await asyncio.sleep(timeout * 3/4)

async def main():
    logging.basicConfig(level=logging.DEBUG)
    log = logging.getLogger("discovery")
    system_bus = await MessageBus(bus_type=dbus_next.BusType.SYSTEM).connect()
    device = await discover(log, system_bus)
    characteristics, epname = await device_to_characteristics(log, system_bus, device)

    await play_with(log, system_bus, characteristics)

    await run_rdproxying(log, epname, characteristics, "coap+tcp://rd.coap.amsuess.com/.well-known/core")

    # finally: await device.call_disconnect()

asyncio.run(main())
