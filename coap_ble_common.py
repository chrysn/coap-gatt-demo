from aiocoap import Message

def encode(msg):
    """Encode a message as prescribed in coap-over-gatt"""
    return bytes([msg.code]) + msg.opt.encode() + (bytes((0xff,)) + msg.payload if msg.payload else b"")

def decode(data):
    """Inverse of encode"""
    m = Message(code=data[0])
    m.payload = m.opt.decode(data[1:])
    return m

coap_us = "8df804b7-3300-496d-9dfa-f8fb40a236bc"
coap_uc = "2a58fc3f-3c62-4ecc-8167-d66d4d9410c2"
