CoAP-over-GATT demo
===================

This is a demo of the `CoAP-over-GATT`_, which is an experimental way of
transporting the `Constrained Application Protocol`_ (CoAP, RFC7252) between a
constrained devices (in this demo running RIOT_) and applications (in this demo
running in a web browser that supports `Web Bluetooth`_, which limits the
choices to Chrome-based browsers).

The constrained part of the application can run on any RIOT supported board_
with support for Bluetooth, and is flashed by having a RIOT checkout available
at the given RIOTBASE and calling::

    make RIOTBASE=${HOME}/RIOT BOARD=my-board-name all flash

The browser part is run by opening the ``demo.html`` file in Chromium and
pressing the button. (An `online version`_ is available as well).

The C and JavaScript versions have abysmally bad CoAP implementations, which can only exchange the
very messages in the demo: Get /.well-known/core, parse it under the assumption
that it contains only a single very simple link, and follow that link to a
plain-text board name to be displayed.

Extended implementations
------------------------

A more complete example of the embedded side can be found in
``coapserver-rs/``, which is built using Rust-on-RIOT_; it can be built in
there by the same command line, provided all build dependencies are present. It
features a more complete CoAP implementation, and other small enhancements.

An alternative client based on Python, aiocoap_ and BlueZ is available in ``client.py``
and executes the same sequence as the JavaScript demo. The ``proxy.py`` example
works asynchronously (depending on dbus-next_), and
does not run the demo on its own, but registers the discovered device at the
public resource directory at rd.coap.amsuess.com_. Thus, the CoAP server is
made available for anyone to request it via the RD's proxy.

.. _`CoAP-over-GATT`: https://tools.ietf.org/html/draft-amsuess-core-coap-over-gatt-00
.. _`Constrained Application Protocol`: https://tools.ietf.org/html/rfc7252
.. _RIOT: https://riot-os.org/
.. _`Web Bluetooth`: https://webbluetoothcg.github.io/web-bluetooth/
.. _board: http://doc.riot-os.org/group__boards.html
.. _Rust-on-RIOT: https://gitlab.com/etonomy/riot-examples
.. _`online version`: https://chrysn.gitlab.io/coap-gatt-demo/
.. _aiocoap: https://aiocoap.readthedocs.io/en/latest/installation.html
.. _dbus-next: https://pypi.org/project/dbus-next/
.. _rd.coap.amsuess.com: https://coap.amsuess.com/

Screenshots
-----------

.. image:: screenshots/pairing.png
   :alt: Browser showing a "localhost:8003 wants to pair" dialog offering "CoAP over GATT – paired" as an option

.. image:: screenshots/result.png
   :alt: Browser showing a conversation transcript:
        Picking a CoAP server...
        Connecting to GATT Server...
        Getting Service...
        Getting Characteristic...
        Sending request for .well-known/core...
        Reading response...
        Got response: </board>
        Sending second request
         01 b5 62 6f 61 72 64
        Waiting for response...
        Response is 45 ff 6e 72 66 35 32 38 34 30 64 6f 6e 67 6c 65
        Textual content: nrf52840dongle

Accesssing a BLE device through ``proxy.py``:

.. raw:: html

   <pre style="overflow:scroll">$ aiocoap-client coap://rd.coap.amsuess.com/resource-lookup/
   <font color="#555753"><b>application/link-format content was re-formatted</b></font>
   &lt;coap://d2c505a7e0c6.proxy.rd.coap.amsuess.com/board&gt;; <font color="#06989A">anchor</font>=<font color="#C4A000">&quot;coap://d2c505a7e0c6.proxy.rd.coap.amsuess.com&quot;</font>,
   &lt;coap://d2c505a7e0c6.proxy.rd.coap.amsuess.com/led&gt;; <font color="#06989A">anchor</font>=<font color="#C4A000">&quot;coap://d2c505a7e0c6.proxy.rd.coap.amsuess.com&quot;</font>
   $ aiocoap-client coap://d2c505a7e0c6.proxy.rd.coap.amsuess.com/board
   nrf52840dongle
   $
   </pre>
