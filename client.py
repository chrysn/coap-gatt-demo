#!/usr/bin/env python3

import dbus
import time
from pprint import pprint
import aiocoap

from coap_ble_common import encode, decode, coap_uc, coap_us

system_bus = dbus.SystemBus()
# FIXME discover the device
adapter = dbus.Interface(system_bus.get_object('org.bluez', '/org/bluez/hci0'), 'org.bluez.Adapter1')
adapter.SetDiscoveryFilter({"UUIDs": [coap_us]})
try:
    adapter.StartDiscovery()
except dbus.exceptions.DBusException as e:
    print("Caght", repr(e))
    print("It's probably org.bluez.Error.InProgress, letting it slide")

# CoAP devices already talked to
found = []

# Try a few times while discovering -- allowing quick results for devices that
# were already discovered, but keep trying while not all may have been found
for i in range(5):
    # Walk through all devices, decide based on primary UUIDs which to connect to
    blueroot = dbus.Interface(system_bus.get_object('org.bluez', '/'), 'org.freedesktop.DBus.ObjectManager')
    for path, items in blueroot.GetManagedObjects().items():
        if 'org.bluez.Device1' not in items:
            continue

        device_properties = items['org.bluez.Device1']
        if coap_us in device_properties['UUIDs']:
            if path in found:
                continue
            found.append(path)
            print("Yes it's a CoAP device")
            print(f"Name is {str(device_properties['Name'])!r}, address {device_properties['Address']}")
            print("Trying to connect")

            device = dbus.Interface(system_bus.get_object('org.bluez', path), 'org.bluez.Device1')

            # we already got that served on a silver plate just before; merely
            # checking as for me to get familiar with the interfaces
            device_properties = dbus.Interface(system_bus.get_object('org.bluez', path), 'org.freedesktop.DBus.Properties')
            assert coap_us in device_properties.Get('org.bluez.Device1', 'UUIDs')

            # Some of this seems to be necessary (it does create the
            # connection), but some of this fails but things still do work after.
            # Leaving it in as it's required when the device is being talked to
            # for the first time.
            if device_properties.Get('org.bluez.Device1', 'Connected') == False:
                print("Not connected, connecting...")
                device.Connect()
                #device.ConnectProfile(coap_us)

            # now we should have descriptors available

            for charpath, charitems in blueroot.GetManagedObjects().items():
                if 'org.bluez.GattCharacteristic1' not in charitems:
                    #print(f"Found something at {charpath} but it's not a GattCharacteristic1")
                    continue

                if charitems['org.bluez.GattCharacteristic1']['UUID'] != coap_uc:
                    #print(f"Found characteristic {path}, but it's not coap_uc")
                    continue

                print(f"There's a characteristic at {charpath} -- I'll speak CoAP to it")

                char = dbus.Interface(system_bus.get_object('org.bluez', charpath), 'org.bluez.GattCharacteristic1')

                req = aiocoap.Message(code=aiocoap.GET, uri_path=[".well-known", "core"])
                req = encode(req)
                char.WriteValue(req, {})
                res = bytes(char.ReadValue({}))
                res = decode(res)
                print(f"Read response: {res} payload {res.payload}")
    time.sleep(1)

print("These were all CoAP devices found in the alotted time.")
adapter.StopDiscovery()
