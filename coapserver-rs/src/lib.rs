#![no_std]
use core::convert::TryInto;

use riot_wrappers::riot_main;
use riot_wrappers::error::NegativeErrorExt as _;

use riot_wrappers::{
    stdio::println,
};

unsafe extern "C" fn gap_event_cb(event: *mut riot_sys::ble_gap_event, _arg: *mut riot_sys::libc::c_void) -> i32
{
    let event = &*event;

    match event.type_.into() {
        riot_sys::BLE_GAP_EVENT_CONNECT => {
            if event.__bindgen_anon_1.connect.status != 0 {
                println!("Connect event was unsuccessful");
                // Start advertising again -- the connection was unsuccessful
                start_advertise();
            } else {
                println!("Successful connect event");
            }
        }
        riot_sys::BLE_GAP_EVENT_DISCONNECT => {
            start_advertise();
            println!("Disconnect event");
        }
        _ => {
            println!("Other event");
        }
    }

    0
}

fn start_advertise() {
    use riot_sys::*;

    /* start to advertise this node */
    let mut advp = riot_sys::ble_gap_adv_params {
        conn_mode: BLE_GAP_CONN_MODE_UND.try_into().unwrap(),
        disc_mode: BLE_GAP_DISC_MODE_GEN.try_into().unwrap(),
        ..Default::default()
    };

    extern "C" {
        // FIXME why necessary?
        static nimble_riot_own_addr_type: u8;
    };
    const BLE_HS_FOREVER: i32 = i32::MAX;
    unsafe {ble_gap_adv_start(nimble_riot_own_addr_type, 0 as _, BLE_HS_FOREVER,
                           &advp, Some(gap_event_cb), 0 as _) }
        .negative_to_error()
            .expect("ble_gap_adv_start failed");
}

struct CoapBleBuffer(heapless::Vec<u8, heapless::consts::U512>);

use coap_message_utils::*;

impl CoapBleBuffer {
    const fn new() -> Self {
        Self(heapless::Vec(heapless::i::Vec::new()))
    }

    fn access_buffer(&mut self) -> &mut heapless::Vec<u8, heapless::consts::U512> {
        &mut self.0
    }

    fn parse<'a>(&'a mut self) -> Option<inmemory::Message<'a>> {
        self.0.get(0)
            .map(|code| *code)
            .map(move |code| inmemory::Message::new(code, &self.0[1..]))
    }

    fn write<'a>(&'a mut self) -> CoapBleWriter<'a> {
        let buf = self.access_buffer();
        buf.clear();
        buf.push(0);
        CoapBleWriter {
            buffer: buf,
            option_base: Some(0)
        }
    }

    /// Workhorse for gatt_svr_chr_access_coap, doing the actual CoAP processing and writing the
    /// reponse back into the buffer; this will eventually drive a coap-handler and is indicating
    /// CoapBleBuffer's way towards being a full CoAP server.
    ///
    /// Errors are currently indicated on the command line, and practically expressed by writing a
    /// sutiable error code back into the buffer.
    fn process_write(&mut self, handler: &mut impl coap_handler::Handler) {
        let extracted = match self.parse() {
            None => {
                self.access_buffer().clear();
                return;
            }
            Some(msg) => handler.extract_request_data(&msg)
        };

        let mut msg = self.write();
        handler.build_response(&mut msg, extracted);
    }
}

struct CoapBleWriter<'a> {
    buffer: &'a mut heapless::Vec<u8, heapless::consts::U512>,
    option_base: Option<u16>,
}

impl<'a> coap_message::MinimalWritableMessage for CoapBleWriter<'a> {
    type Code = u8;
    type OptionNumber = u16;

    fn set_code(&mut self, code: u8) {
        self.buffer[0] = code;
    }

    fn add_option(&mut self, number: u16, data: &[u8]) {
        use coap_message_utils::option_extension::encode_extensions;

        // FIXME error handling?
        let option_base = self.option_base.expect("Payload already set");

        if option_base > number {
            panic!("Wrong option sequence");
        }
        let delta = number - option_base;
        self.option_base = Some(number);
        self.buffer.extend_from_slice(encode_extensions(
                delta,
                data.len().try_into().expect("Data length can't be expressed")
                ).as_ref())
            .unwrap();
        self.buffer.extend_from_slice(data);
    }

    fn set_payload(&mut self, data: &[u8]) {
        if self.option_base.is_none() {
            panic!("Content set twice");
        }
        self.option_base = None;
        if self.buffer.len() != 0 {
           self.buffer.push(0xff);
            self.buffer.extend(data);
        }
    }
}

// FIXME: The coap-handler interface currently only works for MutableWritableMessage -- even though
// the handlers used here don't require that. Short-cutting...
impl<'a> coap_message::MutableWritableMessage for CoapBleWriter<'a> {
    fn available_space(&self) -> usize { todo!() }
    fn payload_mut(&mut self) -> &mut [u8] { todo!() }
    fn truncate(&mut self, _: usize) { todo!() }
    fn mutate_options<F>(&mut self, _: F) { todo!() }
}

static COAP_BUFFER: riot_wrappers::mutex::Mutex<CoapBleBuffer> = riot_wrappers::mutex::Mutex::new(CoapBleBuffer::new());

unsafe extern "C" fn gatt_svr_chr_access_coap(_conn_handle: u16, _attr_handle: u16, ctxt: *mut riot_sys::ble_gatt_access_ctxt, _arg: *mut riot_sys::libc::c_void) -> i32
{
    use riot_sys::*;

    let mut coap_buffer = match COAP_BUFFER.try_lock() {
        Some(b) => b,
        _ => return 1,
    };

    println!("Accessing CoAP characteristic");
    let &context = &*ctxt;

    match context.op.into() {
        BLE_GATT_ACCESS_OP_READ_CHR => {
            println!("read from characteristic");

            let buf = coap_buffer.access_buffer();

            /* send given data to the client */
            os_mbuf_append(context.om, buf.as_ptr() as _,
                                buf.len() as u16)
        }
        BLE_GATT_ACCESS_OP_WRITE_CHR => {
            println!("write to characteristic");

            {
                let buf = coap_buffer.access_buffer();
                /* read sent data */
                let capacity = buf.capacity();
                buf.resize_default(capacity);
                let mut om_len: u16 = 0;
                let ret = ble_hs_mbuf_to_flat(context.om, buf.as_mut_ptr() as _,
                                         buf.len() as _, &mut om_len);
                if ret < 0 {
                    return ret;
                }
                buf.truncate(om_len.into());
            }

            // FIXME build this somewhere else and pass it in
            use coap_handler::implementations::{new_dispatcher, TypedStaticResponse, HandlerBuilder};
            let board = riot_wrappers::board();
            let mut handler = new_dispatcher()
                .at(&[".well-known", "core"], TypedStaticResponse { payload: b"</board>", contentformat: &[40] })
                .at(&["board"], TypedStaticResponse::from(board))
                ;

            coap_buffer.process_write(&mut handler);

            0
        }
        _ => {
            println!("unhandled operation!");
            1
        }
    }
}

riot_main!(main);
fn main() {
    let device_name = cstr::cstr!("CoAP over GATT");
    let device_name_short = "CoAP";

    use riot_wrappers::nimble::uuid::Uuid128;

    let uuid_us: Uuid128 = "8df804b7-3300-496d-9dfa-f8fb40a236bc".parse().unwrap();
    let uuid_uc: Uuid128 = "2a58fc3f-3c62-4ecc-8167-d66d4d9410c2".parse().unwrap();

    /* define several bluetooth services for our device */
    let gatt_svr_svcs_characteristics = [ riot_sys::ble_gatt_chr_def {
                /* Characteristic: Read/Write Demo write */
                uuid: (&uuid_uc).into(),
                access_cb: Some(gatt_svr_chr_access_coap),
                flags: (riot_sys::BLE_GATT_CHR_F_READ | riot_sys::BLE_GATT_CHR_F_WRITE).try_into().unwrap(),
                ..Default::default()
            },
            Default::default() /* No more characteristics */
    ];
    let gatt_svr_svcs = [
        riot_sys::ble_gatt_svc_def {
            /* Service: CoAP demo */
            type_: riot_sys::BLE_GATT_SVC_TYPE_PRIMARY.try_into().unwrap(),
            uuid: (&uuid_us).into(),
            characteristics: gatt_svr_svcs_characteristics.as_ptr(),
            ..Default::default()
        },
        Default::default() /* No more services */
    ];

    println!("NimBLE CoAP-over-GATT example server");

    use riot_sys::*;

    /* verify and add our custom services */
    // unsafe: Function needs to ensure gatt_svr_svcs stays around
    unsafe { ble_gatts_count_cfg(gatt_svr_svcs.as_ptr()) }
        .negative_to_error()
        .expect("ble_gatts_count_cfg failed");

    // unsafe: Function needs to ensure gatt_svr_svcs stays around
    unsafe { ble_gatts_add_svcs(gatt_svr_svcs.as_ptr()) }
        .negative_to_error()
        .expect("ble_gatts_add_Svcs failed");

    /* set the device name */
    // unsafe: device_name stays around
    unsafe { riot_sys::ble_svc_gap_device_name_set(device_name.as_ptr() as _) };
    
    /* reload the GATT server to link our added services */
    // unsafe: all the other functions' pointers are still around
    unsafe { ble_gatts_start() };

    /* configure and set the advertising data */
    let mut ad = riot_wrappers::bluetil::Ad::new_maximal();
    ad.add_flags(riot_sys::BLUETIL_AD_FLAGS_DEFAULT).unwrap();
    ad.add(BLE_GAP_AD_NAME_SHORT, device_name_short.as_bytes()).unwrap();
    ad.add(BLE_GAP_AD_UUID128_COMP, uuid_us.value()).unwrap();
    unsafe {
        let data = ad.destroy();
        ble_gap_adv_set_data(data.as_ptr(), data.len() as _);
    }

    start_advertise();

    loop { riot_wrappers::thread::sleep(); }

    drop(gatt_svr_svcs);
    drop(device_name);
}
