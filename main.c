/*
 * Copyright (C) 2018 Freie Universität Berlin
 *               2018 Codecoup
 *               2020 Christian Amsüss <chrysn@fsfe.org>
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @file
 * @brief       BLE CoAP example using NimBLE
 *
 * This is eavily based on the nimble_gatt example shipped with RIOT-OS, and
 * several parts that are non-essential to the CoAP-over-GATT functionality
 * have not been removed yet.
 *
 * @author      Hauke Petersen <hauke.petersen@fu-berlin.de>
 * @author      Andrzej Kaczmarek <andrzej.kaczmarek@codecoup.pl>
 * @author      Hendrik van Essen <hendrik.ve@fu-berlin.de>
 * @author      Christian Amsüss <chrysn@fsfe.org>
 *
 * @}
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nimble_riot.h"
#include "net/bluetil/ad.h"

#include "host/ble_hs.h"
#include "host/util/util.h"
#include "host/ble_gatt.h"
#include "services/gap/ble_svc_gap.h"
#include "services/gatt/ble_svc_gatt.h"

#define GATT_DEVICE_INFO_UUID                   0x180A
#define GATT_MANUFACTURER_NAME_UUID             0x2A29
#define GATT_MODEL_NUMBER_UUID                  0x2A24

#define STR_ANSWER_BUFFER_SIZE 100

/* UUID US = 8df804b7-3300-496d-9dfa-f8fb40a236bc */
static const ble_uuid128_t uuid_us
        = BLE_UUID128_INIT(0xbc, 0x36, 0xa2, 0x40, 0xfb, 0xf8, 0xfa, 0x9d, 0x6d, 0x49, 0x00, 0x33, 0xb7, 0x04, 0xf8, 0x8d);

/* UUID UC = 2a58fc3f-3c62-4ecc-8167-d66d4d9410c2 */
static const ble_uuid128_t uuid_uc
        = BLE_UUID128_INIT(0xc2, 0x10, 0x94, 0x4d, 0x6d, 0xd6, 0x67, 0x81, 0xcc, 0x4e, 0x62, 0x3c, 0x3f, 0xfc, 0x58, 0x2a);

static const char *device_name = "CoAP over GATT";
static const char *device_name_short = "CoAP";

size_t coap_buffer_valid = 0;
static char coap_buffer[512] = {};

static int gatt_svr_chr_access_coap(
        uint16_t conn_handle, uint16_t attr_handle,
        struct ble_gatt_access_ctxt *ctxt, void *arg);

static void start_advertise(void);

/* define several bluetooth services for our device */
static const struct ble_gatt_svc_def gatt_svr_svcs[] = {
    {
        /* Service: CoAP demo */
        .type = BLE_GATT_SVC_TYPE_PRIMARY,
        .uuid = (ble_uuid_t*) &uuid_us.u,
        .characteristics = (struct ble_gatt_chr_def[]) { {
            /* Characteristic: Read/Write Demo write */
            .uuid = (ble_uuid_t*) &uuid_uc.u,
            .access_cb = gatt_svr_chr_access_coap,
            .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_WRITE,
        }, {
            0, /* No more characteristics in this service */
        }, }
    },
    {
        0, /* No more services */
    },
};

static int gap_event_cb(struct ble_gap_event *event, void *arg)
{
    (void)arg;

    switch (event->type) {
        case BLE_GAP_EVENT_CONNECT:
            if (event->connect.status) {
                puts("Connect event was unsuccessful");
                // Start advertising again -- the connection was unsuccessful
                start_advertise();
            } else {
                puts("Successful connect event");
            }
            break;

        case BLE_GAP_EVENT_DISCONNECT:
            start_advertise();
            puts("Disconnect event");
            break;
        default:
            puts("Other event");
    }

    return 0;
}

static void start_advertise(void)
{
    struct ble_gap_adv_params advp;
    int rc;

    memset(&advp, 0, sizeof advp);
    advp.conn_mode = BLE_GAP_CONN_MODE_UND;
    advp.disc_mode = BLE_GAP_DISC_MODE_GEN;
    rc = ble_gap_adv_start(nimble_riot_own_addr_type, NULL, BLE_HS_FOREVER,
                           &advp, gap_event_cb, NULL);
    assert(rc == 0);
    (void)rc;
}

static int gatt_svr_chr_access_coap(
        uint16_t conn_handle, uint16_t attr_handle,
        struct ble_gatt_access_ctxt *ctxt, void *arg)
{
    (void) conn_handle;
    (void) attr_handle;
    (void) arg;

    int rc = 0;

    puts("Accessing CoAP characteristic");

    switch (ctxt->op) {

        case BLE_GATT_ACCESS_OP_READ_CHR:
            puts("read from characteristic");

            /* send given data to the client */
            rc = os_mbuf_append(ctxt->om, &coap_buffer,
                                coap_buffer_valid);

            break;

        case BLE_GATT_ACCESS_OP_WRITE_CHR:
            puts("write to characteristic");

            uint16_t om_len;
            om_len = OS_MBUF_PKTLEN(ctxt->om);

            /* read sent data */
            rc = ble_hs_mbuf_to_flat(ctxt->om, &coap_buffer,
                                     sizeof coap_buffer, &om_len);
            coap_buffer_valid = om_len;

            if (coap_buffer_valid > 3 && coap_buffer[0] == 0x01 && coap_buffer[1] == 0xbb && coap_buffer[2] == '.') {
                /* that's good enough to assume we want .well-known/core */
                coap_buffer[0] = 0x45; /* 2.05 Content */
                coap_buffer[1] = 0xff; /* payload marker */
                memcpy(&coap_buffer[2], "</board>", 8);
                coap_buffer_valid = 10;
            } else if (coap_buffer_valid > 3 && coap_buffer[0] == 0x01 && coap_buffer[1] == 0xb5 && coap_buffer[2] == 'b') {
               /* looks like a GET /board */
                coap_buffer[0] = 0x45; /* 2.05 Content */
                coap_buffer[1] = 0xff; /* payload marker */
                memcpy(&coap_buffer[2], RIOT_BOARD, strlen(RIOT_BOARD));
                coap_buffer_valid = 2 + strlen(RIOT_BOARD);
            } else {
                coap_buffer[0] = 0x84; /* 4.04 Not Found */
                coap_buffer_valid = 1;
            }

            break;

        default:
            puts("unhandled operation!");
            rc = 1;
            break;
    }

    return rc;
}

int main(void)
{
    puts("NimBLE CoAP-over-GATT example server");

    int rc = 0;
    (void)rc;

    /* verify and add our custom services */
    rc = ble_gatts_count_cfg(gatt_svr_svcs);
    assert(rc == 0);
    rc = ble_gatts_add_svcs(gatt_svr_svcs);
    assert(rc == 0);

    /* set the device name */
    ble_svc_gap_device_name_set(device_name);
    
    /* reload the GATT server to link our added services */
    ble_gatts_start();

    /* configure and set the advertising data */
    uint8_t buf[BLE_HS_ADV_MAX_SZ];
    bluetil_ad_t ad;
    bluetil_ad_init_with_flags(&ad, buf, sizeof(buf), BLUETIL_AD_FLAGS_DEFAULT);
    bluetil_ad_add(&ad, BLE_GAP_AD_NAME_SHORT, device_name_short, strlen(device_name_short));
    bluetil_ad_add(&ad, BLE_GAP_AD_UUID128_COMP, &uuid_us.value, sizeof(uuid_us.value));
    ble_gap_adv_set_data(ad.buf, ad.pos);

    /* start to advertise this node */
    start_advertise();

    return 0;
}
